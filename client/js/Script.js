
// Variables
  var updateTime = 1000;
  var graphSent = [];
  var graphSpel = [];
  var update = 1;
  var line1Sent = [];
  var line2Sent = [];
  var line3Sent = [];
  var line1Spel = [];
  var line2Spel = [];
  var line3Spel = [];
  var range = 5;
  var val1 = 0;
  var val2 = 0;
  var locked = false;
  var x = 0;
  var searches = [];
  var interval;



  // Function to lock the graph scaling
  function changeLock(){
    if(!locked){
      locked = true;
    }else{
      locked = false;
    }
  }



  // Handle graph Scaling
  function changeScale(scale, direction){

    // Go through each graph and change the scale
    for (var i = 0; i < graphSent.length; i++) {

          if(direction == 'min'){
          graphSent[i].options.maxValue = scale;
        }
          if(direction == 'max'){
          graphSent[i].options.minValue = scale;
        }

        // If it is locked, change both at the same time
       if(locked == true){
         if(direction == 'min'){

          document.getElementById("min").value = scale *-1;
           graphSent[i].options.minValue = scale;
           graphSent[i].options.maxValue = scale*-1;

         }else{
           document.getElementById("max").value = scale *-1
           graphSent[i].options.minValue = scale*-1;
           graphSent[i].options.maxValue = scale;
         }
       }
    }
  }



  // Called when a user adds a search term
  function startSearch(){

    // Grab the term from the input box
    var search = document.getElementById("search").value;

    // Replace all '%' with empty character
    search = search.replace("#", "%25");

    search = search.replace(",", "");


    // Empty the input box
    $('#search').val('');

    // Send a request to the server, with the term in the url
    $.get("/search/add/"+search, function(string){
      $("#toptitle").text(string);
    })
  }


  // Adds a new set of graphs to the page
  function addGraph(canvasID){

    // SENTIMENT
    // Creates new lines for the graph
    var lineRec = new TimeSeries();
    var lineHis = new TimeSeries();
    var lineLast = new TimeSeries();

    // Pushes the lines to an array
    line1Sent.push(lineRec);
    line2Sent.push(lineHis);
    line3Sent.push(lineLast);

    // Creates a new smoothie chart, with associated lines attatched
    var smoothie = new SmoothieChart({labels: {fontSize:11,precision:6}, maxValue:5,minValue:-5, grid: { strokeStyle: 'rgb(125, 0, 0)', fillStyle: 'rgb(0, 0, 0)', lineWidth: 1, millisPerLine: 1000, verticalSections: 6,},timestampFormatter:SmoothieChart.timeFormatter});
    smoothie.addTimeSeries(line1Sent[line1Sent.length-1], { strokeStyle: 'rgb(0, 255, 0)', fillStyle: 'rgba(0, 255, 0, 0.4)', lineWidth: 3 });
    smoothie.addTimeSeries(line2Sent[line2Sent.length-1], { strokeStyle: 'rgb(255, 0, 255)', fillStyle: 'rgba(255, 0, 255, 0)', lineWidth: 3 });
    smoothie.addTimeSeries(line3Sent[line3Sent.length-1], { strokeStyle: 'rgb(255, 255, 255)', fillStyle: 'rgba(255, 255, 255, 0)', lineWidth: 1.5 });

    // Begins the streaming of data to the graph
    smoothie.streamTo(document.getElementById("canvasSent"+searches[canvasID]), 1000);

    // Adds the graph to the array of sentiment graphs
    graphSent.push(smoothie);


    // SPELLING
    // Creates new lines for the graph
    lineRec = new TimeSeries();
    lineHis = new TimeSeries();
    lineLast = new TimeSeries();

    // Pushes the lines to an array
    line1Spel.push(lineRec);
    line2Spel.push(lineHis);
    line3Spel.push(lineLast);

    // Creates a new smoothie chart, with associated lines attatched
    var smoothie = new SmoothieChart({labels: {fontSize:11,precision:6}, maxValue:1,minValue:0, grid: { strokeStyle: 'rgb(125, 0, 0)', fillStyle: 'rgb(0, 0, 0)', lineWidth: 1, millisPerLine: 1000, verticalSections: 6,},timestampFormatter:SmoothieChart.timeFormatter});
    smoothie.addTimeSeries(line1Spel[line1Spel.length-1], { strokeStyle: 'rgb(0, 255, 0)', fillStyle: 'rgba(0, 255, 0, 0.4)', lineWidth: 3 });
    smoothie.addTimeSeries(line2Spel[line2Spel.length-1], { strokeStyle: 'rgb(255, 0, 255)', fillStyle: 'rgba(255, 0, 255, 0)', lineWidth: 3 });
    smoothie.addTimeSeries(line3Spel[line3Spel.length-1], { strokeStyle: 'rgb(255, 255, 255)', fillStyle: 'rgba(255, 255, 255, 0)', lineWidth: 1.5 });
    // Begins the streaming of data to the graph
    smoothie.streamTo(document.getElementById("canvasSpel"+searches[canvasID]), 1000);

    // Adds the graph to an array of spelling graphs
    graphSpel.push(smoothie);
  }


// Updates each graph
function updateGraph(){

  // Clear the timer interval
  clearInterval(interval);


  // Asynchronous loop in order to handle server requests
  var asyncLoop = function(o){
      var i=-1,
          length = searches.length;

      var loop = function(){
          i++;
          if(i>=length){o.callback(); return;}
          o.functionToLoop(loop, i);
      }
      loop();//init
  }


// Grabs the average results for spelling and sentiment, applies it to each graph
  asyncLoop({
      functionToLoop : function(loop, i){
          setTimeout(function(){

            var search = searches[i];
            search = search.replace("#", "%25");
            $.get("/Sentiment/"+search, function(string){
                if(line1Sent[i] != null && line1Sent[i] != undefined){
                  line1Sent[i].append(new Date().getTime(), string[0]);
                  line2Sent[i].append(new Date().getTime(), string[1]);
                  line3Sent[i].append(new Date().getTime(), string[2]);
                }

                var search = searches[i];
                search = search.replace("#", "%25");
                $.get("/Spelling/"+search, function(string){
                    if(line1Spel[i] != null && line1Spel[i] != undefined){
                      line1Spel[i].append(new Date().getTime(), string[0]);
                      line2Spel[i].append(new Date().getTime(), string[1]);
                      line3Spel[i].append(new Date().getTime(), string[2]);
                      //console.log(string[2]);
                    }

                    loop();
                    })
                })
          });
      },

      callback : function(){
          interval = setInterval(updateGraph, updateTime);
      }
  });

}


  // Updates the search terms
  function updateSearch(){

    // Requests results from the server
    $.get("/search/terms", function(terms){

      // Check to see if the search terms have changed since last checked
      if(terms.length == searches.length){
        for (var i = 0; i < terms.length; i++) {
          if(terms[i] == searches[i]){
          }else{
            update = 1;
            i = 100;
          }
        }
      }else{
        update = 1;
      }

      // If the terms have changed
      if(update == 1){

        // Set update to 0 and update the search terms
        update = 0;
        searches = terms;

        // Empty the active search box and populate it with the new terms
        $('#activeSearch').empty();
        for (var i = 0; i < searches.length; i++) {
          addSearch(searches[i], i);
        }

        // Remove the graphs
        emptyGraph();

        // Empty the graph holders and populate it with new searches
        $('#graphHolder').empty();
        for (var i = 0; i < searches.length; i++) {
          $("<div class='row'>\
            <div class='col-sm-6'>\
              <div class='panel panel-default'>\
               <div class='panel-heading'>"+searches[i]+" Sentiment</div>\
               <div class='panel-body'>\
             	    <canvas id='canvasSent"+searches[i]+"' width='525px' class='centre'></canvas>\
             	</div>\
              \
             </div>\
            </div>\
            <div class='col-sm-6'>\
              <div class='panel panel-default'>\
               <div class='panel-heading'>"+searches[i]+" Spelling</div>\
               <div class='panel-body'>\
             	    <canvas id='canvasSpel"+searches[i]+"' width='525px' class='centre'></canvas>\
               </div>\
             </div>\
            </div>\
           </div>").appendTo("#graphHolder");
           addGraph(i);
        }
      }
    });

  }


  // Empties the arrays containing the graphs and associated lines
  function emptyGraph(){

    graphSpel = [];
    line1Spel = [];
    line2Spel = [];
    line3Spel = [];
    graphSent = [];
    line1Sent = [];
    line2Sent = [];
    line3Sent = [];
  }

  // Adds a button displaying a search term
  function addSearch(term, index){
    $("<input type='button' onclick='removeSearch("+index+")' value='"+term+"' id='btn_"+index+"'/>").appendTo('#activeSearch');
  }

  // Sends the delete request to the server, and updates the search terms
  function removeSearch(index){

    var search = searches[index]
    search = search.replace("#", "%25");
    $.get("/search/remove/"+search, function(terms){
        updateSearch();
    });
  }

  // Change the polling speed display text
  function requestTime(value){
    updateTime = value;
    $('#poll').empty();
    $('#poll').append('Polling Speed - '+value+'ms');
  }


  // Update the search terms every 250ms
  setInterval(function() {
    updateSearch();
  }, 250);


  // update graph
  updateGraph();
