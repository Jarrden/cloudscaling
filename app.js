var express = require('express');
var app = express();
var http = require('http').Server(app);
const path	= require("path");
var request = require('request');
var fs = require('fs');
var Twitter = require('twitter'); //https://www.npmjs.com/package/twitter
var sentiment = require('sentiment');
var mysql = require('mysql');
var urlencode = require('urlencode');
var Typo = require('typo-js');
var dictionary = new Typo('en_US');
app.use(express.static(__dirname + '/client/'));

// Array of Search Terms
var searchTerms = [];

// SQL Connection Details
var connection = mysql.createConnection({
  host: '35.189.10.150',
  user: 'root',
  password: 'password.123.321', // Pinnacle of security
  database: 'twitterdb',
  charset: 'utf8mb4'
});


// Connect to the database
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);
});


// Sentiment Request
app.get("/Sentiment/:text", function(req, response, body){

  // Grab the search term
  var searchParam = req.params.text;
  searchParam = searchParam.replace("%", "#");

  // Query the database for the most recent 1000 tweets
  connection.query("SELECT * FROM tweets WHERE ID BETWEEN (SELECT MAX(ID) - 1000 from tweets) AND (SELECT MAX(ID) FROM tweets)", function (error, results, fields){
    if(error)
    throw error;

    // Initialise variables
    var searchResults = [];
    var recentSum = 0;
    var historicSum = 0;
    var recentCount = 0;
    var historicCount = 0;
    var sent = 0;
    var lastAv = 0;


    // Determine the sentiment of the most recent 250 Tweets (Recent)
    for (var i = results.length-1; i > results.length- 251; i--) {

      // Set the tweet text and search parameter to lower case and compare them
      var text = results[i].textContents;
      text = text.toLowerCase();
      var param = searchParam.toLowerCase();

      // Determine the sentiment of each tweet
      if(text.includes(param)){
          sent = sentiment(results[i].textContents);

          // Increment the recent sum and count to determine average sentiment
          recentSum += sent.score;
          recentCount ++;
      }
      if(lastAv == 0)
      lastAv = recentSum/recentCount;
    }

    // Database could contain less than 1000 tweets
    // Determine the amount of tweets returned and set it to a length variable
    var histLength;
    if(results.length < 1000){
      histLength = results.length;
    }else{
      histLength = 1000;
    }


    // Go through each tweet, determining sentiment values
    for (var i = 0; i < histLength; i++) {

      // Set each tweet's text and search parameter to lower case for comparison
      var text = results[i].textContents;
      text = text.toLowerCase();
      var param = searchParam.toLowerCase();

      // Determine Sentiment, incrementing historic sum and count for averaging
      if(text.includes(param)){

        sent = sentiment(results[i].textContents);
        historicSum += sent.score;
        historicCount ++;
      }
    }

    // Determine average sentiment for recent tweets
    // if no tweets returned, set avg to 0
    if(recentCount != 0){
      var recAv = recentSum/recentCount;
    }else{
      recAv = 0;
    }


    // Determine average sentiment for historic tweets
    // if no tweets returned, set avg to 0
    if(historicCount != 0){
      var hisAv = historicSum/historicCount;
    }else{
      hisAv = 0;
    }

    // Return sentiment
    response.send([recAv, hisAv, lastAv]);

  })
})


// Average Spelling Request
app.get("/Spelling/:text", function(req, response, body){

  // Grab the search term
  var searchParam = req.params.text;

  searchParam = searchParam.replace("%", "#");

  // Query the databse for the most recent 1000 tweets
  connection.query("SELECT * FROM tweets WHERE ID BETWEEN (SELECT MAX(ID) - 1000 from tweets) AND (SELECT MAX(ID) FROM tweets)", function (error, results, fields){
    if(error)
    throw error;

    // Initialise variables
    var searchResults = [];
    var recentSum = 0;
    var historicSum = 0;
    var recentCount = 0;
    var historicCount = 0;
    var errors = 0;
    var wordCount = 0;
    var recentSpellAv = 0;
    var historicSpellAv = 0;
    var sent = 0;
    var histLength;
    var lastAv = 0;

    // go through the most recent 250 tweets (recent)
    for (var i = results.length-1; i > results.length- 251; i--) {

      // Set the search term and tweet text to lower case for easy comparison
      var text = results[i].textContents;
      text = text.toLowerCase();
      var param = searchParam.toLowerCase();

      // Determine the amount of words with errors for each tweet
      if(text.includes(param)){

        // Split tweets up into an array of words
        var words = text.split(" ");

        // Go through each word and compare it against a dictionary
        // Increment word count and error count where required
        for (var k = 0; k < words.length; k++) {
          wordCount++;
          if(!dictionary.check(words[k]))
          errors++;
        }

        // Determine the recent average, set

        if(lastAv == 0)
        lastAv = errors/wordCount;
      }
    }

    recentSpellAv = errors/wordCount;
    wordCount = 0;
    errors = 0;


    // Database could contain less than 1000 tweets
    // Determine the amount of tweets returned and set it to a length variable
    if(results.length < 1000){
      histLength = results.length;
    }else{
      histLength = 1000;
    }


    // Go through each tweet, determining the average spelling error percentage
    for (var i = 0; i < histLength; i++) {

      // Set search parameter and tweet text to lowercase for easy comparison
      var text = results[i].textContents;
      text = text.toLowerCase();
      var param = searchParam.toLowerCase();


      if(text.includes(param)){

        // Split the tweet into an array of words and then determine the amount
        // of errors and words.
        var words = text.split(" ");
        for (var k = 0; k < words.length; k++) {
          wordCount++;
          if(!dictionary.check(words[k]))
          errors++;
        }

      }
    }

    // Determine the average spelling error rate for historic tweets
     historicSpellAv = errors/wordCount;

     // Return average spelling error rates
    response.send([recentSpellAv, historicSpellAv, lastAv]);
  })
})


// Adds a search to the search terms list
function addSearch(search){

  // Determine if the term is already present in the list
  var isPresent = false;
  for (var i = 0; i < searchTerms.length; i++) {
    if(search == searchTerms[i])
    isPresent = true;
  }

  // If it isn't present, add it to the list
  if(!isPresent)
  searchTerms.push(search);

  // Combine the search terms into a query that can be used by the stream
  combineSearch();
}


// Removes a search from the list of searches
function removeSearch(search){

  // Determine if the search term is present, noting the index
  var isPresent = false;
  var index = -1;
  for (var i = 0; i < searchTerms.length; i++) {
      if(search == searchTerms[i]){
      isPresent = true;
      index = i;
    }
  }

  // If it is present, remove the search term from the list using the index
  if(isPresent){
    searchTerms.splice(index, 1);
    combineSearch();
  }
}


// Combines the search terms into a string that can be used by the Streaming
// Application

function combineSearch(){

  // Go through each search term and add it to the query string
  var searchQuery = '';
  for (var i = 0; i < searchTerms.length; i++) {
    searchQuery += searchTerms[i];

    // If it is not the last word, append ',' to the string
    if(i != searchTerms.length - 1)
    searchQuery += ',';
  }

  // Add the search term to the database
   connection.query("INSERT INTO searchparam (param) VALUES(?)",[searchQuery], function(err){
     if(err)
     throw err;
   });

}

// Split the search query into an array of search terms if the list isn't empty
function splitSearch(search){
  if(search != ''){
    searchTerms = search.split(',');
  }else{
    searchTerms = [];
  }
}


// Request for the search terms
app.get("/searches", function(req, response, body){
  send.response(searchTerms);
})


// Request to add a search term
app.get("/search/add/:text", function(req, response, body){

  // Add the search term


  var searchParam = req.params.text;
  searchParam = searchParam.replace("%","#");
  addSearch(searchParam);

  // Send a response to prevent client from timing out
  response.send([1]);
})


// Request to remove search term
app.get("/search/remove/:text", function(req, response, body){
  // Remove search term
  var searchParam = req.params.text;
  searchParam = searchParam.replace("%","#");
  removeSearch(searchParam);

  // Send a response to prevent the client from timing out
  response.send([1]);
})


// Request for the list of search terms
// Grabs the most recent search term from database to prevent other processing
// servers from falling behind
app.get("/search/terms", function(req, response, body){

  // Grab the most recent search query from the database
  connection.query('SELECT * FROM searchparam WHERE ID = (SELECT MAX(ID) FROM searchparam)', function (error, results, fields){
    if(error)
    throw error;

    // Split the search into an array of terms and send to client
    splitSearch(results[0].param);
    response.send(searchTerms);
  });
})


// Update search parameters every 250ms
setInterval(function(){

  // Grab the most recent search query from the dabase
  connection.query('SELECT * FROM searchparam WHERE ID = (SELECT MAX(ID) FROM searchparam)', function (error, results, fields){
    if(error)
    throw error;

    // Split the query into an array of search terms
    splitSearch(results[0].param);
  });
}, 250)


// Start server on port 80
app.listen(80, function() {
	console.log('Server Started on port 80');
});
