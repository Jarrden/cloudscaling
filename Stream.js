var fs = require('fs');
var Twitter = require('twitter'); //https://www.npmjs.com/package/twitter
var mysql = require('mysql');

var searchID = -1;
var stream;
var count = 0;
var searchText = '';


// Database Connection Details
var connection = mysql.createConnection({
  host: '35.189.10.150',
  user: 'root',
  password: 'password.123.321',
  database: 'twitterdb',
  charset: 'utf8mb4'
});


// Connect to database
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);
});


// Grab twitter details from external file
var twitterDetails = fs.readFileSync('twitterDetails.json');
twitterDetails = JSON.parse(twitterDetails);


// Setup the twitter client
var client = new Twitter({
  consumer_key: twitterDetails.consumer_key,
  consumer_secret: twitterDetails.consumer_secret,
  access_token_key: twitterDetails.access_token_key,
  access_token_secret: twitterDetails.access_token_secret
});


// Kills the search
function killSearch(){
  if(stream != null){
    stream.destroy();
    console.log("search Killed");
  }
}


// Starts the search
function startSearch(searchTerm){
  console.log("searching for: " + searchTerm);

  // Creates a new stream with supplied searchTerm
  stream = client.stream('statuses/filter', {track: searchTerm, language: 'en', delimited: 'length', stall_warnings: 'true'});

  // When the stream recieves a tweet
  stream.on('data', function(event) {

    // A response with event.text is typically a tweet
    if(event.text != undefined && event.text != null){

      // Insert the tweet into the database.
      connection.query("INSERT INTO tweets (textcontents, recievedtime) VALUES(?,?)",[event.text,event.timestamp_ms], function(err){
        if(err)
        throw err;
      });
    }
  });

  // If the stream returns an error, handle it
  //
  //
  //
  //
  //
  //
  //
  stream.on('error', function(error) {
      console.log("Stream has died, waiting to reconnect");
      killSearch();
      setTimeout(function(){
        console.log("Starting Search");
        startSearch(searchText);
      }, 5000);

    });
}


// Every 5 seconds, update the search term
setInterval(function(){

  // Grab the most recent search query from the database
  connection.query('SELECT * FROM searchparam WHERE ID = (SELECT MAX(ID) FROM searchparam)', function (error, results, fields){
    if(error)
    throw error;

    // If the search isn't in use
    if(results[0].inUse == 'no' && results[0].ID != searchID){

      // Set the searchID to the result's ID
      searchID = results[0].ID;

      // Kill the search
      killSearch();

      // If the search is empty, there is no need to start the search
      if(results[0].param != '')
      searchText = results[0].param;
      startSearch(results[0].param);
    }
  });
}, 5000)


console.log("Starting Streaming Service");
